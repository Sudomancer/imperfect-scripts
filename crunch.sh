#!/bin/bash -x
RATE=""
while getopts 'r:' flag; do
    case "$flag" in
        r) RATE="${OPTARG}" ;;
        *) echo "bad flag" ;;
    esac
done

COMMAND="youtube-dl --write-sub --sub-lang 'enUS' --sub-format 'best/ass/srt' --reject-title '(.*dub.*)/gmi' -o '%(series)s/%(season)s/%(series)s - s%(season_number)se%(episode_number)s - %(episode)s' --netrc \"${@: -1}\""

if [ ! -z "$RATE" ]; then
    echo "$RATE is not empty"
    COMMAND="$COMMAND -r $RATE"
fi

eval "$COMMAND"
