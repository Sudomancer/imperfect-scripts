#!/usr/bin/bash
GREEN="$(tput setaf 2)"
BOLD="$(tput bold)"
case "$1" in
    --help|-h) # Asking for help
        echo "Usage:
        cht.sh                       interactive mode
        cht.sh <program>    single command cheatsheet"
        ;;
        "")
        while read -p "${BOLD}Cheat: ${GREEN}" cmd; do # Interactive mode
            tput sgr0 # Clear text effects
            curl -Gs "cht.sh/$cmd"
        done
        echo "Exiting..."
        ;;
    *)
        INPUT="$@"
        curl -Gs "cht.sh/${INPUT}"
        ;;
esac
